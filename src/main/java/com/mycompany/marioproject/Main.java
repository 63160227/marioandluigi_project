/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.marioproject;

/**
 *
 * @author a
 */
public class Main {
    public static void main(String[] args) {
        Mario mario = new Mario("Mario","red",120,50);
        Luigi luigi = new Luigi("Luigi","green",139,38);
        
        mario.eatmushrooms();
        mario.tall();
        mario.tall(luigi.tall);
        mario.jump();
        System.out.println("___________________");
        
        luigi.eatmushrooms();
        luigi.tall();
        luigi.tall(mario.tall);
        luigi.jump();
        
        
    }
}
