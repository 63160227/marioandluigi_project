/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.marioproject;

/**
 *
 * @author a
 */
public class Mario extends Character {
    public Mario(String name, String color,int tall,int size) {
        super(name,color,tall,size);

    }

    public void Name() {
        System.out.println("Mario : " + name);
    }

    public void tall() {
        System.out.println("Mario : " + tall + " cm");
    }
    public void tall(int tallcheck) {
        if(tall < tallcheck) 
            System.out.println("Mario less tall than " + tallcheck + " cm");
        else 
            System.out.println("Mario greater tall than " + tallcheck + " cm");
    }
    @Override
    public void eatmushrooms() {
        System.out.println("Mario : " + name + " eatmushrooms!!" ); 
    }

    /**
     *
     */
    @Override
    public void jump() {
        System.out.println("Mario " + name + " jump up !!"); 
}
}