/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.marioproject;

/**
 *
 * @author a
 */
public class Character {

    protected String name;
    protected int tall = 0;
    protected String color;
    protected int size = 0;

    public Character(String name, String color, int tall, int size) {
        System.out.println("Character created");
        this.name = name;
        this.color = color;
        this.tall = tall;
        this.size = size;

    }

    public void jump() {
        System.out.println("Mario and Luigi jump!!");
    }

    public void eatmushrooms() {
        System.out.println("Mario and Luigi eatmushrooms");
        System.out.println("Name : " + this.name + " color : " + this.color
        + " tall : " + this.tall + "cm" + this.size + "!!");

    }

    public String getName() {
        return name;
    }

    public int getTall() {
        return tall;
    }

    public String getColor() {
        return color;
    }

    public int getSize() {
        return size;
    }

}